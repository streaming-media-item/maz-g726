# 使用 MAZ-G.7726 编码 G.726

输入 PCM 文件，输出 G.726 编码文件。

## 编译

```
paul@vmware:~/gitee/maz-g726/demo/encode$ make
gcc -I./ -I../../  -c encode.c -o encode.o
gcc -I./ -I../../  -c ../../maz_cpnt_g726.c -o ../../maz_cpnt_g726.o
gcc encode.o ../../maz_cpnt_g726.o -I./ -I../../  -o encode
paul@vmware:~/gitee/maz-g726/demo/encode$ 
```

 ## 编码

```
./encode 2 dayu_8k_1ch_s16le.pcm 2.g726
ffplay -f g726le -ar 8000 -ac 1 -code_size 2 -i 2.g726 // 杂音多
ffplay -f g726 -ar 8000 -ac 1 -code_size 2 -i 2.g726   // 杂音少

./encode 3 dayu_8k_1ch_s16le.pcm 3.g726
ffplay -f g726le -ar 8000 -ac 1 -code_size 3 -i 3.g726 // 完全不能听
ffplay -f g726 -ar 8000 -ac 1 -code_size 3 -i 3.g726   // 完全正常

./encode 4 dayu_8k_1ch_s16le.pcm 4.g726
ffplay -f g726le -ar 8000 -ac 1 -code_size 4 -i 4.g726 // 杂音少
ffplay -f g726 -ar 8000 -ac 1 -code_size 4 -i 4.g726   // 完全正常

./encode 5 dayu_8k_1ch_s16le.pcm 5.g726
ffplay -f g726le -ar 8000 -ac 1 -code_size 5 -i 5.g726 // 完全不能听
ffplay -f g726 -ar 8000 -ac 1 -code_size 5 -i 5.g726   // 完全正常
```

说明，平台的字节序是小端序，因为我送小端序的 PCM 音频进去可以正常编码并播放。

因此输入的数据要是小端PCM序。至于输出，从播放效果来看，g726库输出的是按照大端序输出。

